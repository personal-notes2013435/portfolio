<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Inertia\Inertia;

class IndexController extends Controller
{
   public function home(){
    return inertia('Users/index/UserHome');
   }
   
   public function adminhome(){
    return inertia('Admin/Index/AdminHome');
   }
}


