<?php

use App\Http\Controllers\AdminNewsController;
use App\Http\Controllers\AllUserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;

Route::get('/home',[IndexController::class, 'home']);



//admin route goes here
Route::prefix('admin')
  ->name('admin.')
  ->middleware('auth')
  ->group(function (){
    Route::get('/',[IndexController::class, 'adminhome']);
   
  });


Route::get('/', function () {
  $user = Auth::user();
  if($user){
    if($user->role === 'admin'){
      return redirect('/admin');
    } 
  }

  return redirect('/home');
});
