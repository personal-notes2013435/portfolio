import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import MainLayout from './Layouts/MainLayout.vue'
import AdminLayout from './Layouts/AdminLayout.vue'
// import MapLayout from './Layouts/MapLayout.vue'
import '../css/app.css'

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
    let page = pages[`./Pages/${name}.vue`]

    
    if(name.startsWith('Users/')){
      page.default.layout = page.default.layout || MainLayout
    }
    if(name.startsWith('Admin/')){
      page.default.layout = page.default.layout || AdminLayout
    }
    return page
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .mount(el)
  },
})